/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 异常工具类
 * @author huzhihui
 * @version v 0.1 2017/5/11 22:09 huzhihui Exp $$
 */
public class ExceptionUtils {

    /**
     * 从exception中获取错误日志
     * @param e 异常信息对象
     * @return  转换为字符串的异常信息
     */
    public static String getExceptionString(Exception e) {
        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer, true));
        return writer.toString();
    }
}
