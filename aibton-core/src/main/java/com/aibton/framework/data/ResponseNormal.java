/**
 * Aibton.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package com.aibton.framework.data;

import com.aibton.framework.config.AibtonConstantKey;

/**
 * 普通返回工具类
 * @author huzhihui
 * @version v 0.1 2017/4/9 18:23 huzhihui Exp $$
 */
public class ResponseNormal<T> extends BaseResponse {
    /**
     * 是否请求成功：true：false
     */
    private boolean success = false;

    /**
     * 返回code
     */
    private String  code    = AibtonConstantKey.RESPONSE_000000;

    /**
     * 返回的bean
     */
    private T       msg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }

    /**
     * 普通返回
     * @param success   状态值
     * @param msg   返回信息
     * @param <T>   泛型对象
     * @return  返回包装对象
     */
    public static <T> ResponseNormal<T> getData(boolean success, T msg) {
        ResponseNormal<T> responseNormal = new ResponseNormal<T>();
        responseNormal.setSuccess(success);
        responseNormal.setMsg(msg);
        return responseNormal;
    }

    /**
     *
     * @param success   状态值
     * @param code  code编码
     * @param msg   返回信息
     * @param <T>   泛型对象
     * @return  返回包装对象
     */
    public static <T> ResponseNormal<T> getData(boolean success, String code, T msg) {
        ResponseNormal<T> responseNormal = new ResponseNormal<T>();
        responseNormal.setSuccess(success);
        responseNormal.setCode(code);
        responseNormal.setMsg(msg);
        return responseNormal;
    }
}
