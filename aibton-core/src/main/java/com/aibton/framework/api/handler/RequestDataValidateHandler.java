/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handler;

import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.util.AibtonValidateUtils;

/**
 * 入参校验
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/28 10:34 huzhihui Exp $$
 */
public class RequestDataValidateHandler implements IBaseApiHandler {

    @Override
    public void doHandel(EngineContext engineContext) {
        AibtonValidateUtils.validate(engineContext.getBaseRequest());
    }
}
