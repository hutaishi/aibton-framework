/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handler;

import com.aibton.framework.api.auth.util.AibtonAuthUtils;
import com.aibton.framework.api.data.EngineContext;

/**
 * API接口访问权限校验处理器
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/28 11:34 huzhihui Exp $$
 */
public class AibtonAuthValidateHandler implements IBaseApiHandler {

    @Override
    public void doHandel(EngineContext engineContext) {
        AibtonAuthUtils.validateAuth(engineContext.getAbstractBaseApi());
    }
}
