/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.threadLocal;

import java.util.List;

import com.aibton.framework.api.auth.data.AuthData;
import com.aibton.framework.config.AibtonConstantKey;
import com.aibton.framework.util.HttpServletRequestUtils;

/**
 * Api 当前线程副本
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/24 15:58 huzhihui Exp $$
 */
public class ApiThreadLocalUtils {

    /**
     * 权限列表
     */
    private static ThreadLocal<List<AuthData>> authDatas = new ThreadLocal<>();

    /**
     * 获取权限列表
     * @return  权限列表值
     */
    public static List<AuthData> getAuthDatas() {
        return authDatas.get();
    }

    /**
     * 设置权限列表
     * @param authDatas 权限列表对象
     */
    public static void setAuthDatas(List<AuthData> authDatas) {
        ApiThreadLocalUtils.authDatas.set(authDatas);
    }

    /**
     * 设置权限列表到session中
     * @param authDatas 设置权限列表
     */
    public static void setAuthsSession(List<AuthData> authDatas) {
        HttpServletRequestUtils.getHttpSession().setAttribute(AibtonConstantKey.AIBTON_AUTH_DATAS,
            authDatas);
    }

    /**
     * 获取权限列表
     * @return  权限列表值
     */
    public static List<AuthData> getAuthsSession() {
        if (null != HttpServletRequestUtils.getHttpSession()
            .getAttribute(AibtonConstantKey.AIBTON_AUTH_DATAS)) {
            return (List<AuthData>) HttpServletRequestUtils.getHttpSession()
                .getAttribute(AibtonConstantKey.AIBTON_AUTH_DATAS);
        }
        return null;
    }
}
