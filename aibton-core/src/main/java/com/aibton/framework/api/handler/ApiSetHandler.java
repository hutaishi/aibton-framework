/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.api.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aibton.framework.api.AbstractBaseApi;
import com.aibton.framework.api.config.AibtonApiConstantKey;
import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.api.ioc.AibtonApiIoc;
import com.aibton.framework.util.AssertUtils;

/**
 * Api反射处理器
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/21 9:35 huzhihui Exp $$
 */
public class ApiSetHandler implements IBaseApiHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiSetHandler.class);

    @Override
    public void doHandel(EngineContext engineContext) {
        AssertUtils.isNotNull(LOGGER, engineContext, AibtonApiConstantKey.ENGINE_CONTEXT_NULL);
        AssertUtils.isNotEmpty(LOGGER, engineContext.getApiUrl(),
            AibtonApiConstantKey.API_URL_Null);
        AbstractBaseApi abstractBaseApi = AibtonApiIoc.getApiBean(engineContext.getApiUrl());
        AssertUtils.isNotNull(LOGGER, abstractBaseApi, AibtonApiConstantKey.API_NULL);
        engineContext.setAbstractBaseApi(abstractBaseApi);
    }
}
