package com.aibton.framework.api;

import com.aibton.framework.api.data.EngineContext;
import com.aibton.framework.data.BaseResponse;

/**
 * API执行引擎主入口
 * @author huzhihui
 * @version $: v 0.1 2017 2017/7/20 15:19 huzhihui Exp $$
 */
public interface IApiEngine {

    BaseResponse run(EngineContext engineContext);
}
