/**
 * Aibton.com Inc.
 * Copyright (c) 2016-2017 All Rights Reserved.
 */
package com.aibton.framework.config;

import java.nio.charset.Charset;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.aibton.framework.api.ApiEngineImpl;
import com.aibton.framework.api.ApiInitProcessor;
import com.aibton.framework.api.IApiEngine;
import com.aibton.framework.util.RestTemplateUtils;
import com.aibton.framework.util.SpringContextUtils;

/**
 * 框架配置类
 * @author huzhihui
 * @version $: v 0.1 2017 2017/8/22 10:08 huzhihui Exp $$
 */
@Configuration
public class AibtonConfig {

    @Bean
    public RestTemplate restTemplate() {
        StringHttpMessageConverter m = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> httpMessageConverters = restTemplate.getMessageConverters();
        for (int i = 0; i < httpMessageConverters.size(); i++) {
            if (httpMessageConverters.get(i) instanceof StringHttpMessageConverter) {
                httpMessageConverters.set(i, m);
                break;
            }
        }
        restTemplate.setMessageConverters(httpMessageConverters);
        return restTemplate;
    }

    @Bean
    public SpringContextUtils getSpringContextUtils() {
        return new SpringContextUtils();
    }

    @Bean
    public RestTemplateUtils getRestTemplateUtils() {
        return new RestTemplateUtils();
    }

    @Bean
    public IApiEngine getIApiEngine() {
        return new ApiEngineImpl();
    }

    @Bean
    public ApiInitProcessor getApiInitProcessor() {
        return new ApiInitProcessor();
    }
}
